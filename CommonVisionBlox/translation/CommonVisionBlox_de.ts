<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>CVBInterface</name>
    <message>
        <location filename="../commonVisionBloxInterface.cpp" line="+66"/>
        <source>This plugin can connect to various cameras via the GenICam interface of the commercial tool Common Vision Blox from company Stemmer. 
 
 Until now, the plugin is only implemented for monochrome pixel formats mono8, mono10, mono12, mono14 and mono16. Besides the ROI and 
 exposure time, all parameters need to be read and set using the parameter raw:suffix where suffix is the real GenICam parameter, obtained 
 via the Stemmer configuration tool. If a bitdepth &gt; 8 bit is chosen, an error might occur during acquisition. Then check the indicated ini file 
 from Stemmer GenICam and don&apos;t set the pixelFormat property to auto but Mono16. 
 
 In case of a slow connection, check the communication center of Stemmer for hints or bugs in the connection, e.g. use the filter driver for GigE connections. 
 
 This plugin has been tested with DALSA Genie HM1400 and Xenics Bobcat 640 GigE.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Licensed under LGPL, Stemmer Common Vision Blox under its own license.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>N.A.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>If 1 scan for new cameras, else take the last opened camera (default). If you scan for new cameras, the configuration file (ini) created in CommonVisionBlox for GenICam or other cameras will be reset to the default values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>desired monochromatic bitdepth. Dependent on this parameter PixelFormat is set to mono8, mono10, mono12, mono14 or mono16. Make sure the bitdepth is supported by your camera</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommonVisionBlox</name>
    <message>
        <location filename="../commonVisionBlox.cpp" line="+53"/>
        <source>Exposure time of chip (in seconds).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Heartbeat timeout of GigE Vision Transport Layer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&apos;snap&apos; is a single image acquisition (only possible in trigger_mode &apos;off&apos;), &apos;grab&apos; is a continuous acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&apos;off&apos;: camera is not explicitly triggered but operated in freerun mode. The next acquired image is provided upon acquire, &apos;software&apos; sets trigger mode to On and fires a software trigger at acquire (only possible in acquisition_mode &apos;grab&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Pixelsize in x (cols)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pixelsize in y (rows)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>bit depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>left end of the ROI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>right end of the ROI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>upper end of the ROI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>downer end of the ROI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>use raw:paramname to set internal paramname of camera to value. paramname is the original GenICam parameter name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>vendor name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>model name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>ROI (x,y,width,height) [this replaces the values x0,x1,y0,y1]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+744"/>
        <source>StopDevice of CommonVisionBlox can not be executed, since camera has not been started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Acquire of CommonVisionBlox can not be executed, since camera has not been started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+188"/>
        <source>data object of getVal is NULL or cast failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Empty object handle retrieved from caller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>image could not be obtained since no image has been acquired.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../commonVisionBloxInterface.cpp" line="-37"/>
        <source>GenICam cameras via Common Vision Blox from Stemmer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
