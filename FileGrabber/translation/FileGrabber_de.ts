<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DockWidgetFileGrabber</name>
    <message>
        <location filename="../dockWidgetFileGrabber.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>General Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>[ID]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Image Dimensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Bits per Pixel:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+19"/>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source> bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+33"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Integrationtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileGrabber</name>
    <message>
        <location filename="../FileGrabber.cpp" line="+235"/>
        <source>Integrationtime of CCD programmed in s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Time between two frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Virtual gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Virtual offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Binning of different pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Pixelsize in x (cols)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-8"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Pixelsize in y (rows)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Grabdepth of the images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Timeout for acquiring images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The current shown image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The maximal number if images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+942"/>
        <source>Unable to load file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+132"/>
        <source>Nice try but wrong turn.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-889"/>
        <source>parameter not found in m_params.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-86"/>
        <source>name of given parameter is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Parameter is read only, input ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>New value is larger than parameter range, input ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New value is smaller than parameter range, input ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Parameter type conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+488"/>
        <source>StopDevice of FileGrabber can not be executed, since camera has not been started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Acquire of FileGrabber can not be executed, since camera has not been started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>data object of getVal is NULL or cast failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Empty object handle retrieved from caller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+172"/>
        <source>getVal of FileGrabber failed, since undefined bitdepth.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location line="-1087"/>
        <source>A virtual grabber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>This plugin emulates a camera by grabbing images from files in a specific folder on the hard disk. Alternatively, it is possible to iteratively load images from a 3D-data object (stack). The first possibility uses the command imread from OpenCV (OpenCV library highgui required). 

The grabber can work in 3 different modes: 
1) Files in a specified folder are sequentially loaded each time a getVal / copyVal is performed. At the moment only 8 and 16 bit images are supported.
2) Images are sequentially loaded from a 3D data object (stack of images). Each getVal / copyVal returns a reference/copy to the specific plane of the stack. Supports 8, 12, 14, 16, 24-bit.
3) Files in a specified folder are scanned and preloaded to an image stack. Each getVal / copyVal returns a reference/copy to the specific plane of the stack. Supports 8, 12, 14, 16, 24-bit.

In the second case, provide the objectStack argument, arguments bpp and sourceFolder are ignored. bpp is generated by the type and value range of the objectStack.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Licensed under LPGL.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>N.A.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>File-Type, e.g. &apos;*.tif&apos;, &apos;*.png&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Absolute path of the source images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Destination bit depth. 0: Auto or 8, 12, 14, 16, 24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If 0, no preloading is active, else the first n image are loaded to a stack.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>If not NULL and preloading is active, an 3D-Object can to used for the grabber.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+429"/>
        <source>Folder %1 does not contain any matching files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Folder %1 does not exist or is not readable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Stack object must be 3 dimensional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Stack object type must be UInt8, UInt16 or Int32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Image loading failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>No suitable images found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialogFileGrabber</name>
    <message>
        <location filename="../dialogFileGrabber.cpp" line="+28"/>
        <source>Configuration Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogFileGrabber.ui" line="+14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+150"/>
        <source>Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <location line="+55"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Integrationtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+35"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Frametime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>X0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Y0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>X1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Y1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Set X Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>XSize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>YSize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Set Y Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-483"/>
        <source>Buffer and Binning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Binning X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <location line="+34"/>
        <source>BitPerPix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Binning Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>30</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+382"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
