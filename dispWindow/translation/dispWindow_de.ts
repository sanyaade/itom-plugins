<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DialogDispWindow</name>
    <message>
        <location filename="../dialogDispWindow.ui" line="+14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Window size</source>
        <translation type="unfinished">Fenstermaße</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>x0:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>y0:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>x size:</source>
        <translation type="unfinished">x-Größe:</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>y size:</source>
        <translation type="unfinished">y-Größe:</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Window settings</source>
        <translation type="unfinished">Fenstereinstellungen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cosine period:</source>
        <translation type="unfinished">Cosinus-Periode:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source># phase shifts:</source>
        <translation type="unfinished">Phasenschiebungen:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Color:</source>
        <translation type="unfinished">Farbe:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>red</source>
        <translation type="unfinished">rot</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>green</source>
        <translation type="unfinished">grün</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>blue</source>
        <translation type="unfinished">blau</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>white</source>
        <translation type="unfinished">weiß</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Orientation:</source>
        <translation type="unfinished">Ausrichtung:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>vertical</source>
        <translation type="unfinished">vertikal</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>horizontal</source>
        <translation type="unfinished">horizontal</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Gamma correction:</source>
        <translation type="unfinished">Gammakorrektur:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Show image</source>
        <translation type="unfinished">Anzeigebild</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Image #:</source>
        <translation type="unfinished">Bildnummer:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogDispWindow.cpp" line="+69"/>
        <source>Configuration Dialog</source>
        <translation type="unfinished">Konfigurationsdialog</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Error while configuring projection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Warning while configuring projection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DispWindow</name>
    <message>
        <location filename="../dispWindow.cpp" line="+186"/>
        <source>mean grey values from intensity calibration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>0: Red, 1: Green, 2: Blue, 3: White</source>
        <translation>0: rot, 1: grün, 2: blau, 3: weiß</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>0: vertical, 1: horizontal; default: vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Number of different images: Phaseshift + GrayCode + 2</source>
        <translation type="unfinished">Anzahl der unterschiedlichen Bilder: Phasenschiebung + Grauwert + 2</translation>
    </message>
    <message>
        <location line="+229"/>
        <source>lut has wrong size, 256 values required!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-274"/>
        <source>Calculate lookup-table for the calibration between projected grayvalue and the registered camera intensity (maps 256 gray-values to its respective mean ccd values, see parameter &apos;lut&apos;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>absolute filename of the file where the grabbing image should be saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>grab the current OpenGL frame as image and saves it to the given filename. The image format is guessed from the suffix of the filename (default QImage formats supported)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>unique gray value. Depending on the projected color, all color channels are reduced by this value [0..255]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>projects an image where all pixels have the same gray-level. This is used for the determination of the gamma correction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>name of the plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cosine period in pixel. This must be a multiple of 2 and the number of &apos;phaseshift&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Count of phase shifts. If this value is changed and the &apos;period&apos; does not fit to the new value, the &apos;period&apos; is adapted to the next possible value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>x0 position of display window [px]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>y0 position of display window [px]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>width of window [px]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>height of window [px]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Number of current image (phase images, dark image, bright image, graycode images)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>0: disable gamma correction, 1: enable gamma correction; default disable (see also &apos;lut&apos;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lookup table for a gamma correction with 256 values. The gamma correction itself is en-/disabled via parameter &apos;gamma&apos;. If enabled, the value to display is modified by lut[value]. Per default the lut is a 1:1 relation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>DataObject with pixel values to display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Supported OpenGL Version is lower than 2.0 and therefore not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Currently the lut only has %i values, therefore the index must be in the range [0,%i]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+160"/>
        <source>mandatory or optional parameters vector not initialized!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+124"/>
        <source>wrong z-size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>wrong x-size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>wrong y-size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>wrong data type (uint8) required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+22"/>
        <location line="+30"/>
        <location line="+22"/>
        <source>cosine image uninitialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-33"/>
        <location line="+52"/>
        <source>wrong image number - internal error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+50"/>
        <source>insufficient gray values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+93"/>
        <source>timeout while grabbing current OpenGL frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>timeout while projecting a single-color image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>function name &apos;%s&apos; does not exist</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DispWindowInterface</name>
    <message>
        <location line="-970"/>
        <source>Window for SLM/LCD-Applications</source>
        <translation type="unfinished">Fenster für SLM/LCD-Anwendungen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>LGPL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>x0 position of window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>y0 position of window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>height of window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>width of window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>cosine period in pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>number of total phase shifts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lookup table for a gamma correction with 256 values. If given, the gamma correction will be enabled (default: off) and the projected values are then modified with lut[value].</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>DataObject with pixel values to display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-18"/>
        <source>N.A.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockWidgetDispWindow</name>
    <message>
        <location filename="../dockWidgetDispWindow.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>General Information</source>
        <translation>Allgemeine Informationen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>ID:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>[ID]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Show image</source>
        <translation>Anzeigebild</translation>
    </message>
    <message>
        <location filename="../dockWidgetDispWindow.cpp" line="+61"/>
        <source>phase shift %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>white</source>
        <translation type="unfinished">weiß</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>gray images %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrjWindow</name>
    <message>
        <location filename="../projWindow.cpp" line="+1091"/>
        <source>error out of memory (cosine init 7)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>error gen texture (cosine init)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>error gen texture (graycode / cosine init)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1159"/>
        <source>DataObject must not be NULL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location line="-1257"/>
        <source>error out of memory (cosine init 1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>error out of memory (cosine init 2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>error out of memory (cosine init 3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>error out of memory (cosine init 4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>error out of memory (cosine init 5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>error out of memory (cosine init 6)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dispWindow.cpp" line="-13"/>
        <source>This plugin opens a borderless window at a given position and displays horizontal or vertical cosine fringes including various graycode fringes (for unwrapping). The visualization is done with the help of OpenGL and the open source library GLEW. 

For building this plugin, download (the binaries) of glew from http://glew.sourceforge.net/ and set the variable GLEW_DIR in CMake to the corresponding folder. The necessary library will finally be copied to the lib-folder of itom such that an entry in the environment variable path is not necessary. Please make sure, that you use always the same version of glew for all plugins that require this library.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialogDispWindow</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>red</source>
        <translation type="obsolete">rot</translation>
    </message>
    <message>
        <source>green</source>
        <translation type="obsolete">grün</translation>
    </message>
    <message>
        <source>blue</source>
        <translation type="obsolete">blau</translation>
    </message>
    <message>
        <source>white</source>
        <translation type="obsolete">weiß</translation>
    </message>
    <message>
        <source>Window settings</source>
        <translation type="obsolete">Fenstereinstellungen</translation>
    </message>
    <message>
        <source>vertical</source>
        <translation type="obsolete">vertikal</translation>
    </message>
    <message>
        <source>horizontal</source>
        <translation type="obsolete">horizontal</translation>
    </message>
    <message>
        <source>Show image</source>
        <translation type="obsolete">Anzeigebild</translation>
    </message>
    <message>
        <source>Configuration Dialog</source>
        <translation type="obsolete">Konfigurationsdialog</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Übernehmen</translation>
    </message>
    <message>
        <source>Window size</source>
        <translation type="obsolete">Fenstermaße</translation>
    </message>
    <message>
        <source>x size:</source>
        <translation type="obsolete">x-Größe:</translation>
    </message>
    <message>
        <source>y size:</source>
        <translation type="obsolete">y-Größe:</translation>
    </message>
    <message>
        <source>Image #:</source>
        <translation type="obsolete">Bildnummer:</translation>
    </message>
    <message>
        <source>Gamma correction:</source>
        <translation type="obsolete">Gammakorrektur:</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation type="obsolete">Farbe:</translation>
    </message>
    <message>
        <source>Orientation:</source>
        <translation type="obsolete">Ausrichtung:</translation>
    </message>
    <message>
        <source>Cosine period:</source>
        <translation type="obsolete">Cosinus-Periode:</translation>
    </message>
    <message>
        <source># phase shifts:</source>
        <translation type="obsolete">Phasenschiebungen:</translation>
    </message>
</context>
</TS>
