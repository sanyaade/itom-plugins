<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DialogPIHexapodCtrl</name>
    <message>
        <location filename="../dialogPIHexapodCtrl.cpp" line="+24"/>
        <source>Configuration Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+43"/>
        <source>the upper position limit must be higher than the lower one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+69"/>
        <source>timeout while setting parameters of plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>plugin instance not defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Invalid parameter input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Error while setting parameters of plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogPIHexapodCtrl.ui" line="+14"/>
        <source>PI Piezo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+13"/>
        <location line="+23"/>
        <source>[unknown]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Piezo:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Asynchrone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Position limits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>low:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+23"/>
        <source> µm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>high:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Delay after positioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>the total delay is offset + proportional * distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Delay (Offset)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source> ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Delay (Proportional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source> ms/µm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockWidgetPIHexapodCtrl</name>
    <message>
        <location filename="../dockWidgetPIHexapodCtrl.ui" line="+14"/>
        <source>PI Piezo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>General Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+13"/>
        <location line="+23"/>
        <source>[unknown]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Piezo:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>[ID]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Positioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Step Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+149"/>
        <location line="+42"/>
        <source> µm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-155"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Absolut Positioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Actual Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Target Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PIHexapodCtrl</name>
    <message>
        <location filename="../PIHexapodCtrl.cpp" line="+186"/>
        <source>Current type of controller, e.g. E-662, E-665, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>device information string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hexapod information string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>asychronous (1.0) or sychronous (0.0) mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Number of axes (here always 1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>use this parameter followed by :YourCommand in order to read/write value from/to device (e.g. PI_CMD:ERR?)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>name of requested parameter is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>parameter PI_CMD requires the real command send to the motor after a colon-sign.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Array addressing failed, index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Array addressing failed, parameter is no array type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+50"/>
        <source>name of given parameter is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>parameter PI_CMD requires the real command send to the motor after a colon-sign in the parameter name or as value (second parameter of setParam method).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>System has only global speed either try speed[0] or [speed, speed, ...]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Parameter is read only, input ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>New value is larger than parameter range, input ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New value is smaller than parameter range, input ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Given parameter and m_param do not have the same type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>parameter not found in m_params.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Doesn&apos;t fit to interface DataIO!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+107"/>
        <source>you can only calilbrate axis [6], [7], [6,7] or [0-5]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+52"/>
        <source>not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Axis does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+991"/>
        <source>no connection opened!
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+128"/>
        <source>coordinate name &apos;%c&apos; does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>function name &apos;%s&apos; does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-738"/>
        <source>could not read endline parameter from serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-34"/>
        <location line="+62"/>
        <source>timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1020"/>
        <location line="+10"/>
        <source>Position of the Pivot-Point in x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-9"/>
        <location line="+10"/>
        <source>Position of the Pivot-Point in y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-9"/>
        <location line="+10"/>
        <source>Position of the Pivot-Point in z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Set the system Pivot-Point (origin of rotiation)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Get the system Pivot-Point (origin of rotiation)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cycle to iteralte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amplitude in mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wait between to command in seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print the current positions of the specified axis to the consol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+581"/>
        <source>Error. Too many Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error. Wrong Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error. Addressed axis twice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+676"/>
        <source>value could not be parsed to a double value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+57"/>
        <source>could not identify controller. No answer for command *idn?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PIHexapodCtrlInterface</name>
    <message>
        <location line="-1414"/>
        <source>An opened serial port (IF connected via Serial-Port).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The IP-Address of the PI-Controller (If connected via TCP-IP).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The TCP/IP-Port of the PI-Controller (If connected via TCP-IP).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location line="-16"/>
        <source>PI Hexapods H810, H824, H840, H850</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The PIHexapodCtrl is an itom-plugin, which can be used to communicate with PI Hexapod-controllers.
Different PI-Hexapod Controller (E-816, E-621, E-625, E-665 or E662) are implemented,
It is used to work with Piefocs and Hexapod-Stages. The ITO-Controllers have only one axis with axis number 0.
This system needs a serial port, which differs depending on the controller type. The parameter are set automatically during initialization.
It is initialized by actuator(&quot;PIHexapodCtrl&quot;, SerialIO, Controller Type (e.g. &apos;E662&apos;)).
Stageparamters can be set directly by setParam(&quot;STAGEPARAMETER&quot;, value).
WARNING: The calibration of voltage to position are hardcoded into the controller according to its corresponding stage.
Hence, stages should not be mixed up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LGPL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>N.A.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <location filename="../../../../build/itom/SDK/include/common/addInInterface.h" line="+1023"/>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>uninitialized vector for output parameters!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
