<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DemoAlgorithms</name>
    <message>
        <location filename="../demoAlgorithms.cpp" line="+106"/>
        <source>Demo algorithm (I) for plugin-developers - actuator communication. Moves selected axes of an actuator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Demo algorithm (II) for plugin-developers - camera communication. Snaps a single image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Demo algorithm (III) for plugin-developers - camera communication. Snaps a number of images to a stack.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Demo algorithm (IV) for plugin-developers - actuator communication. Moves first axis of an actuator several time to test the actuator performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Demo widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+168"/>
        <location line="+320"/>
        <source>Handle to the Motorstage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-318"/>
        <source>Number of the first axis to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New position of the 1. axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Number of the second axis to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New Position of axis 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Algorithm will change speed to this value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+62"/>
        <location line="+120"/>
        <source>Handle to the Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-118"/>
        <source>Empty object, will contain 2D image later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>If 1, some calculations are simulated during the wait for the camera thread in order to show a time efficient approach.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Image handle empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Recorded via demoSnapImage-filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Empty resulting data object. Contains 3d data object with acquired data after call.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Number of images to aquire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Movie handle empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Grabber bit depth not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Recorded via demoSnapMovie-filter in [s]: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Delta for the current axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Number of iterations to move.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Toggle between abs (0) and relative (1) movements</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DemoAlgorithmsInterface</name>
    <message>
        <location line="-563"/>
        <source>Fill in about dialog content</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location line="-7"/>
        <source>DemoAlgorithms to show a plugin developer how to write plugins in c++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The DemoAlgorithms-DLL contains some basic filter function to show a plugin developer how to use a motor or programm an own plugin widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>LGPL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
